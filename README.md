# Polygon Side

Creates a polygon with a given side length.

Should work for user units (mm, in) or pixels.

Also creates arc circles with n-nodes

Circles with less than 4 nodes will be corrected !

Appears under 'Extensions > Render > Polygon Side'
