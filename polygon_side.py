#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Polygon Side - An Inkscape 1.1+ extension
# Create a Polygon given a number of sides and side length.
#

import math
import inkex
from inkex import PathElement
from inkex import units
import re


# Formula to find the circumradius ( centre to apex ) required to create
# for a given number of sectors to return desired side length

def radius_from_side_and_sectors(self, side_length, sectors, found_units, unit_choice):
    conversions = {
        'in': 96.0,
        'pt': 1.3333333333333333,
        'px': 1.0,
        'mm': 3.779527559055118,
        'cm': 37.79527559055118,
        'm': 3779.527559055118,
        'km': 3779527.559055118,
        'Q': 0.94488188976378,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0,
        '': 1.0,  # Default px
    }

    cf = conversions[unit_choice] / conversions[self.svg.unit]

    radius = (side_length / (2 * (math.sin(math.pi / sectors)))) * cf

    path = svg_poly(self, 0, 0, radius, sectors)

    arc_d = svg_poly_circle(0, 0, radius, sectors)

    return path, arc_d


# All points of a regular polygon lie on a circle
# Calculate points on circle for given number of sides

def svg_poly(self, cx, cy, radius, sectors):

    rotation_corrections = float(self.options.angle_correction_combo)

    # Check for odd or even sectors
    if sectors % 2 == 0:
        angle_correction = 0.5 * rotation_corrections
    if sectors % 2 > 0:
        angle_correction = 0.25 * rotation_corrections

    shift_rotation = self.options.shift_rotation_float

    angle = ((2 * math.pi) / sectors) * angle_correction + shift_rotation
    angle_shift = ((2 * math.pi) / sectors) * angle_correction + shift_rotation

    y_start = cy / 2 + (radius * (math.sin(angle)))
    x_start = cx / 2 + (radius * (math.cos(angle)))

    path = f'M {x_start} {y_start}'

    for sector in range(1, sectors + 1):
        angle = ((sector * math.pi) / (sectors / 2)) + angle_shift

        # inkex.errormsg(f'Sector {sector} Angle {angle}')

        y = cy / 2 + (radius * (math.sin(angle)))
        x = cx / 2 + (radius * (math.cos(angle)))

        path = path + f' L {x} {y} '

    return path + ' z'

def svg_poly_circle(cx, cy, radius, sectors):

    angle = 0

    y_start = cy / 2 + (radius * (math.sin(angle)))
    x_start = cx / 2 + (radius * (math.cos(angle)))

    path = f'M {x_start} {y_start}'

    arc_radius1 = radius
    arc_radius2 = radius
    arc_x_rotate = 0
    arc_large_flag = 0
    arc_sweep_flag = 1


    for sector in range(1, sectors):

        angle = (sector * math.pi) / (sectors / 2)

        y = cy / 2 + (radius * (math.sin(angle)))
        x = cx / 2 + (radius * (math.cos(angle)))

        path = path + f' A {arc_radius1} {arc_radius2} {arc_x_rotate} {arc_large_flag} {arc_sweep_flag} {x} {y}'

    path = path + f' A {arc_radius1} {arc_radius2} {arc_x_rotate} {arc_large_flag} {arc_sweep_flag} {x_start} {y_start}'

    return path + ' z'


class makepoly(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--polygon_side_notebook", type=str, dest="polygon_side_notebook", default=0)

        pars.add_argument("--number_of_sides", type=int, dest="sectors", default=6)
        pars.add_argument("--length_of_sides", type=float, dest="side_length", default=25)
        pars.add_argument("--unit_choice", type=str, dest="unit_choice", default='px')

        pars.add_argument("--rotation_correction_combo", type=str, dest="angle_correction_combo", default='0')
        pars.add_argument("--shift_rotation_float", type=float, dest="shift_rotation_float", default=0)

        pars.add_argument("--output_type_radio", type=str, dest="output_type_radio", default='polygon')


    def effect(self):

        # Get units
        found_units = self.svg.unit


        path, arc_d = radius_from_side_and_sectors(self, self.options.side_length, self.options.sectors, found_units,
                                            self.options.unit_choice)


        style = {'stroke': '#000000', 'fill': 'none', 'stroke-width': str(self.svg.unittouu('1px'))}
        poly_path = PathElement()
        poly_path.style = style
        poly_path.path = path

        arc_path = PathElement()
        arc_path.style = style
        arc_path.path = arc_d

        parent = self.svg.get_current_layer()

        output_type = self.options.output_type_radio

        if output_type == 'polygon':
            parent.append(poly_path)

        if output_type == 'circle':
            parent.append(arc_path)

        if output_type == 'both':
            parent.append(poly_path)
            parent.append(arc_path)


if __name__ == '__main__':
    makepoly().run()
